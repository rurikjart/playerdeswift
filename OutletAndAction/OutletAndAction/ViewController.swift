//
//  ViewController.swift
//  OutletAndAction
//
//  Created by Yury Artyukhov on 14.01.2020.
//  Copyright © 2020 UT. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    @IBOutlet var labelArray: UILabel!
    
    @IBAction func changeTextInLabel(_ sender: UIButton) {
        print(label.text!)
        label.text = "Hello, world!!!"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func SayInConsole(_ sender: UIButton) {
        print("Hello, world! in the console!")
    }
    

}

