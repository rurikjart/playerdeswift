//
//  ContentPageViewController.swift
//  UIPageViewR
//
//  Created by Admin on 10.06.2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class ContentPageViewController: UIViewController {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var emojiLabel: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var presenText = ""
    var emoji = ""
    var currentPage = 0 // Номер текущей страницы
    var numberOfPages = 0 // Количество страниц
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        label.text = presenText
        emojiLabel.text = emoji
        pageControl.numberOfPages = numberOfPages
        pageControl.currentPage = currentPage
    }

}
