//
//  ViewController.swift
//  UIPageViewR
//
//  Created by Admin on 10.06.2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        startPresentation()
       /* if let pageViewController = storyboard?.instantiateViewController(
            withIdentifier: "PageViewController") as? PageViewController {
            
            self.present(pageViewController, animated: true, completion: nil)
        }*/
    }

    // Презентация при первом запуске
    func startPresentation() {
        
        let userDefaults = UserDefaults.standard
        let appAlreadeSeen = userDefaults.bool(forKey: "appAlreadeSeen")
        if appAlreadeSeen == false {

            // Отображение PageViewController
            if let pageViewController = storyboard?.instantiateViewController(
                withIdentifier: "PageViewController") as? PageViewController {

                self.present(pageViewController, animated: true, completion: nil)
            }
        }
    }


}

