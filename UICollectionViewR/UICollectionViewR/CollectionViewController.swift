//
//  CollectionViewController.swift
//  UICollectionViewR
//
//  Created by Admin on 02.06.2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class CollectionViewController: UICollectionViewController {
    
    let coverImages = ["Alberto Ruiz - 7 Elements (Original Mix)",
                             "Dave Wincent - Red Eye (Original Mix)",
                             "E-Spectro - End Station (Original Mix)",
                             "Edna Ann - Phasma (Konstantin Yoodza Remix)",
                             "Ilija Djokovic - Delusion (Original Mix)",
                             "John Baptiste - Mycelium (Original Mix)",
                             "Lane 8 - Fingerprint (Original Mix)",
                             "Mac Vaughn - Pink Is My Favorite Color (Alex Stein Remix)",
                             "Metodi Hristov, Gallya - Badmash (Original Mix)",
                             "Veerus, Maxie Devine - Nightmare (Original Mix)"]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
      //  self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }
    
    
  /*  override func numberOfSections(in collectionView: UICollectionView) -> Int {
           // #warning Incomplete implementation, return the number of sections
           return 0
       }*/
   


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return coverImages.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
    
        // Configure the cell
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CollectionViewCell
        
        cell.coverImageView.image = UIImage(named: coverImages[indexPath.row])
    
        return cell
    }
    
   
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cellDVC = coverImages[indexPath.row]
        self.performSegue(withIdentifier: "CollectionCellD", sender: cellDVC)
    }
    
    //этот метод ошибочный пока не нашел решение
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CollectionCellD" {
            let dv = segue.destination as! DetailsCellViewController
            dv.cellTitle = sender as! String
        }
    }
    
    

   
}
